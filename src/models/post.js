module.exports = (sequelize, type) => sequelize.define('post', {
  id: {
    type: type.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: type.STRING,
  },
  body: {
    type: type.STRING,
  },
  username: {
    type: type.STRING,
  },
  image: {
    type: type.STRING,
  },
});
